package com.itbeta.teste.aguinaldo.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.itbeta.teste.aguinaldo.models.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	Optional<User> findByUsername(String username);
	Optional<User> findByEmail(String email);
	Optional<User> findByToken(String token);

	Boolean existsByUsername(String username);

	Boolean existsByEmail(String email);
}
