package com.itbeta.teste.aguinaldo.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.itbeta.teste.aguinaldo.models.ERole;
import com.itbeta.teste.aguinaldo.models.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
	Optional<Role> findByName(ERole name);
}
