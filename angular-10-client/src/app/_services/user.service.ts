import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const API_URL = 'http://localhost:8080/api/test/';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  getPublicContent(): Observable<any> {
    return this.http.get(API_URL + 'all', { responseType: 'text' });
  }

  getUserBoard(): Observable<any> {
    return this.http.get(API_URL + 'user', { responseType: 'text' });
  }

  getModeratorBoard(): Observable<any> {
    return this.http.get(API_URL + 'mod', { responseType: 'text' });
  }

  getAdminBoard(): Observable<any> {
    return this.http.get(API_URL + 'admin', { responseType: 'text' });
  }

  getCadastrarUsuario(): Observable<any> {
    return this.http.get(API_URL + 'cadastrar_usuario', { responseType: 'text' });
  }

  getEditarUsuario(id): Observable<any> {
    return this.http.get(API_URL + 'editar_usuario/'+id, { responseType: 'text' });
  }

  getResetarSenha(token): Observable<any> {
    return this.http.get(API_URL + 'resetar_senha/'+token, { responseType: 'text' });
  }

  getUsuarios(): Observable<any> {
    return this.http.get(API_URL + 'usuarios', { responseType: 'json' });
  }
}
