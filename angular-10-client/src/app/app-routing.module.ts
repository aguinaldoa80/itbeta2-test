import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegisterComponent } from './register/register.component';
import { CadastrarUsuarioComponent } from './cadastrar_usuario/cadastrar_usuario.component';
import { EditarUsuarioComponent } from './editar_usuario/editar_usuario.component';
import { ReseteSenhaComponent } from './resete_senha/resete_senha.component';
import { ResetarSenhaComponent } from './resetar_senha/resetar_senha.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { BoardUserComponent } from './board-user/board-user.component';
import { BoardModeratorComponent } from './board-moderator/board-moderator.component';
import { BoardAdminComponent } from './board-admin/board-admin.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'cadastrar_usuario', component: CadastrarUsuarioComponent },
  { path: 'editar_usuario/:id', component: EditarUsuarioComponent },
  { path: 'resete_senha/:id', component: ReseteSenhaComponent },
  { path: 'resetar_senha/:token', component: ResetarSenhaComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'user', component: BoardUserComponent },
  { path: 'mod', component: BoardModeratorComponent },
  { path: 'admin', component: BoardAdminComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
