import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReseteSenhaComponent } from './resete_senha.component';

describe('ReseteSenhaComponent', () => {
  let component: ReseteSenhaComponent;
  let fixture: ComponentFixture<ReseteSenhaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReseteSenhaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReseteSenhaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
