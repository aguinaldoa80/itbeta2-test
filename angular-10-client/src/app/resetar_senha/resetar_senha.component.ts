import { Component, OnInit } from '@angular/core';
import { UserService } from '../_services/user.service';
import { AuthService } from '../_services/auth.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './resetar_senha.component.html',
  styleUrls: ['./resetar_senha.component.css']
})
export class ResetarSenhaComponent implements OnInit {

  form: any = {};
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';
  content: string;
  usuario: any;

  constructor(private route: ActivatedRoute, private authService: AuthService, private userService: UserService) { }

  ngOnInit(): void {
    const token = this.route.snapshot.paramMap.get('token');
    this.userService.getResetarSenha(token).subscribe(
      data => {
        this.form = JSON.parse(data);
      },
      err => {
        this.form = JSON.parse(err.error).message;
      }
    );
  }

  onSubmit(): void {
    this.authService.redefinir(this.form).subscribe(
      data => {
        //console.log(data);
        this.isSuccessful = true;
        this.isSignUpFailed = false;
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
      }
    );
  }

}
